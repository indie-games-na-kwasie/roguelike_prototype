﻿using System.Collections.Generic;
using UnityEngine;

public class DungeonCrawler {
    private Vector2Int position;

    public Vector2Int Position {
        get => position;
        set => position = value;
    }

    public DungeonCrawler(Vector2Int startPosition) {
        position = startPosition;
    }

    public Vector2Int Move(Dictionary<Direction, Vector2Int> directionMovementMap) {
        Direction toMove = (Direction) Random.Range(0, directionMovementMap.Count);
        position += directionMovementMap[toMove];

        return position;
    }
}
