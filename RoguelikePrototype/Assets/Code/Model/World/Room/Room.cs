﻿using System.Collections.Generic;
using UnityEngine;


public enum RoomType {
   Normal, Boss, Checkpoint, Start
}

public class Room {
   public static readonly int Width = 25;
   public static readonly int Height = 19;
   
   private RoomType roomType;
   private int x = 0;
   private int y = 0;
   
   public List<Door> Doors = new List<Door>();

   public int X {
      get => x;
      set => x = value;
   }

   public int Y {
      get => y;
      set => y = value;
   }
   
   public RoomType RoomType {
      get => roomType;
      set => roomType = value;
   }

   public Room(int x, int y, RoomType roomType) {
      this.x = x;
      this.y = y;
      this.roomType = roomType;
   }
   
   public Room GetNorthNeighbour() {
      if (RoomManager.Instance.DoesRoomExist(X, Y + Height)) { return RoomManager.Instance.FindRoomData(X, Y + Height); }
      else { return null; }
   }
   
   public Room GetEastNeighbour() {
      if (RoomManager.Instance.DoesRoomExist(X + Width, Y)) { return RoomManager.Instance.FindRoomData(X + Width, Y); }
      else { return null; }
   }
   
   public Room GetWestNeighbour() {
      if (RoomManager.Instance.DoesRoomExist(X - Width, Y)) { return RoomManager.Instance.FindRoomData(X - Width, Y); }
      else { return null; }
   }
   
   public Room GetSouthNeighbour() {
      if (RoomManager.Instance.DoesRoomExist(X, Y - Height)) { return RoomManager.Instance.FindRoomData(X, Y - Height); }
      else { return null; }
   }

   public Vector3 GetRoomCentre() {
      return new Vector3(X, Y);
   }


}
