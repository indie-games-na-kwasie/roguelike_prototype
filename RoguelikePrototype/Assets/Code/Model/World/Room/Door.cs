﻿public enum DoorType {
    N, E, W, S
}

public class Door {
    
    private DoorType doorType;

    public DoorType DoorType {
        get => doorType;
        set => doorType = value;
    }

    private string name;

    public string Name {
        get => name;
        set => name = value;
    }
}
