﻿using UnityEngine;

public enum enemyType
{
    STABLE,
    STABLERANGED,
    STABLEMELE,
    NONSTABLE,
    NONSTABLERANGED,
    NONSTABLEMELE,
    NONSTABLEFOLLOWMELE,
    NONSTABLEFOLLOWRANGED
}

public class Enemy : ScriptableObject
{
    public string enemyName;
    public int enemyDamage;
    public float enemySpeed;
    public float enemyHealth;
    public enemyType type;
}
