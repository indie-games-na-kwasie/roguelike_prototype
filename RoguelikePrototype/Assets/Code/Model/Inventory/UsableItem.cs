﻿using UnityEngine;

[CreateAssetMenu]
public class UsableItem : Item
{
    public int healthBack;
    public UsableItem()
    {
        itemType = ItemTypeEnum.USABLE;
    }
}
