﻿using UnityEngine;

[CreateAssetMenu]
public class Weapon : Item
{
    public float WeaponDamage;
    public bool IsMeleWeapon;

    public Weapon()
    {
        itemType = ItemTypeEnum.WEAPON;
    }
}
