﻿using UnityEngine;

public enum ItemTypeEnum
{
    WEAPON,
    USABLE
}

[CreateAssetMenu]
public class Item : ScriptableObject
{
    public ItemTypeEnum itemType;
    public string ItemName;
    public Sprite ItemImage;
    public float ItemWeight;
    public int ItemValue;
}
