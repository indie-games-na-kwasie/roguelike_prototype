﻿using UnityEngine;

public class CameraCollider : MonoBehaviour {
   public static bool firstRoom = true;
   
   private void OnTriggerEnter2D(Collider2D roomGuest) {
      if (roomGuest.CompareTag("Player")) {
         Room currentRoom = RoomManager.Instance.FindRoomData(transform.gameObject);

         if (firstRoom) {
            transform.gameObject.GetComponent<RoomController>().UnlockRoom();
            firstRoom = false;
         }
         
         if (currentRoom != null) {
            RoomManager.Instance.OnPlayerEnterRoom(currentRoom);
         }
         else {
            Debug.LogError("CameraCollider:: OnTriggerEnter2D -- Couldn't find existing room to set camera above it.");
         }

      }
   }
}
