﻿using UnityEngine;

public class CameraController : MonoBehaviour {

    public static CameraController Instance;

    private Room currentRoom;

    [SerializeField] private float positionChanceSpeed;

    public Room CurrentRoom {
        get => currentRoom;
        set => currentRoom = value;
    }

    public float PositionChanceSpeed {
        get => positionChanceSpeed;
        set => positionChanceSpeed = value;
    }

    // Start is called before the first frame update
    void Start() {
        Instance = this;
    }

    // Update is called once per frame
    void Update() {
        UpdatePosition();
    }

    private void UpdatePosition() {
        if (currentRoom == null) { return; }

        Vector3 targetPosition = GetCameraTargetPosition();
        transform.position =
            Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * positionChanceSpeed);
    }

    private Vector3 GetCameraTargetPosition() {
        if (currentRoom == null) {
            return Vector3.zero;
        }

        Vector3 cameraPosition = currentRoom.GetRoomCentre();
        cameraPosition.z = transform.position.z;
        return cameraPosition;
    }

    public void SetStartingPosition(Room room) {
        transform.position = new Vector3(room.X, room.Y, transform.position.z);
    }
    
}
