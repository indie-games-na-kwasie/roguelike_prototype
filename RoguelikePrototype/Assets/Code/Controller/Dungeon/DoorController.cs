﻿using UnityEngine;

public class DoorController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision) {
        if (collision.CompareTag("Player")) {
            transform.parent.transform.parent.GetComponent<RoomController>().CloseRoom();
        }
        else if (collision.CompareTag("Bullet")) {
            Destroy(collision);
        }
    }
}
