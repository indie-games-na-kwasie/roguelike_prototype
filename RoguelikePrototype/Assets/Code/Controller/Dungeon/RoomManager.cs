﻿using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour {

    [Header("Player")]
    [SerializeField] private GameObject Player = null;
    
    [Header("Equipment")]
    [SerializeField] private GameObject meleeWeapon = null;
    [SerializeField] private GameObject rangedWeapon = null;
    [SerializeField] private GameObject potion = null;
    
    public static RoomManager Instance;

    private List<Vector2Int> roomsInDungeonCoordinates;
    private List<Room> loadedDungeonRoomsData = new List<Room>();
    private Dictionary<Room, GameObject> dungeonRoomsMap = new Dictionary<Room, GameObject>();

    private Room currentRoom;
    
    public List<Vector2Int> RoomsInDungeonCoordinates {
        get => roomsInDungeonCoordinates;
        set => roomsInDungeonCoordinates = value;
    }

    private void Awake() {
        Instance = this;
    }

    private void Start() {
        DistinctRooms();
        LoadRoomsData();
        SpawnRooms();
    }

    private void DistinctRooms() {
        List<Vector2Int> tmp = new List<Vector2Int>(roomsInDungeonCoordinates);
        roomsInDungeonCoordinates = new List<Vector2Int>();

        foreach (Vector2Int coordinates in tmp) {
            if (!roomsInDungeonCoordinates.Contains(coordinates)) { roomsInDungeonCoordinates.Add(coordinates); }
        }
        
        if (roomsInDungeonCoordinates.Count < 15) {
            Debug.LogError("Small dungeon generated.");
        }
        
        if (!roomsInDungeonCoordinates.Contains(new Vector2Int(0, 0))) {
            SecureCentralDungeonRoom();
        }
    }

    private void LoadRoomsData() {
        foreach (Vector2Int coordinates in roomsInDungeonCoordinates) {
            loadedDungeonRoomsData.Add(new Room(coordinates.x * Room.Width, coordinates.y * Room.Height, RoomType.Normal));
        }
        
        SetStartingRoom(loadedDungeonRoomsData[0]);
    }

    private void SpawnRooms() {
        foreach (Room roomData in loadedDungeonRoomsData) {
            string roomName = "Room_";

            if (roomData.GetNorthNeighbour() != null) { roomName += "N"; }
            if (roomData.GetEastNeighbour() != null) { roomName += "E"; }
            if (roomData.GetWestNeighbour() != null) { roomName += "W"; }
            if (roomData.GetSouthNeighbour() != null) { roomName += "S"; }
            
            GameObject roomGO = (GameObject) Instantiate(Resources.Load("Prefabs/Rooms/" + roomName), transform, true);
            roomGO.transform.position = new Vector3(roomData.X, roomData.Y, 0);
            SetWallColliderTag(roomGO);
            roomGO.name = roomName + "_" + roomData.X + "_" + roomData.Y;

            dungeonRoomsMap[roomData] = roomGO;
        }

        dungeonRoomsMap[currentRoom].GetComponent<RoomController>().EnemyAmount = 0;
        EnemyManager.Instance.GetEnemies();
    }

    private void SetStartingRoom(Room startingRoom) {
        startingRoom.RoomType = RoomType.Start;
        currentRoom = startingRoom;
        
        SetCameraStartingPosition();
        SetPlayerPosition();
    }

    private void SecureCentralDungeonRoom() {
        if (roomsInDungeonCoordinates.Contains(new Vector2Int(-1, 0)) && 
            roomsInDungeonCoordinates.Contains(new Vector2Int(0, 1))) {
            
            if (!roomsInDungeonCoordinates.Contains(new Vector2Int(-1, 1))) {
                roomsInDungeonCoordinates.Add(new Vector2Int(0, 0));
            }
            
        } else if (roomsInDungeonCoordinates.Contains(new Vector2Int(1, 0)) &&
                   roomsInDungeonCoordinates.Contains(new Vector2Int(0, 1))) {
            
            if (!roomsInDungeonCoordinates.Contains(new Vector2Int(1, 1))) {
                roomsInDungeonCoordinates.Add(new Vector2Int(0, 0));
            }
            
        } else if (roomsInDungeonCoordinates.Contains(new Vector2Int(1, 0)) &&
                   roomsInDungeonCoordinates.Contains(new Vector2Int(0, -1))) {
            
            if (!roomsInDungeonCoordinates.Contains(new Vector2Int(1, -1))) {
                roomsInDungeonCoordinates.Add(new Vector2Int(0, 0));
            }
            
        } else if (roomsInDungeonCoordinates.Contains(new Vector2Int(0, -1)) &&
                   roomsInDungeonCoordinates.Contains(new Vector2Int(-1, 0))) {
            
            if (!roomsInDungeonCoordinates.Contains(new Vector2Int(-1, -1))) {
                roomsInDungeonCoordinates.Add(new Vector2Int(0, 0));
            }
            
        } else if (roomsInDungeonCoordinates.Contains(new Vector2Int(0, 1)) &&
                   roomsInDungeonCoordinates.Contains(new Vector2Int(0, -1))) {
            
            roomsInDungeonCoordinates.Add(new Vector2Int(0, 0));
            
        } else if (roomsInDungeonCoordinates.Contains(new Vector2Int(-1, 0)) &&
                   roomsInDungeonCoordinates.Contains(new Vector2Int(1, 0))) {
            
            roomsInDungeonCoordinates.Add(new Vector2Int(0, 0));
        }
    }
    
    public bool DoesRoomExist(int x, int y) {
        return loadedDungeonRoomsData.Find(item => item.X == x && item.Y == y) != null;    
        }

    public Room FindRoomData(int x, int y) {
        return loadedDungeonRoomsData.Find(item => item.X == x && item.Y == y);
    }

    public Room FindRoomData(GameObject soughtRoomGO) {
        foreach (Room roomData in dungeonRoomsMap.Keys) {
            if (dungeonRoomsMap[roomData] == soughtRoomGO) { return roomData; }
        }

        return null;
    }

    public GameObject FindRoomGO(Room room) {
        foreach (Room roomKey in dungeonRoomsMap.Keys) {
            if (roomKey == room) { return dungeonRoomsMap[room]; }
        }

        return null;
    }

    private void SetCameraStartingPosition() {
        CameraController.Instance.SetStartingPosition(currentRoom);
    }
    
    private void SetPlayerPosition() {
        Player.transform.position = new Vector3(currentRoom.X, currentRoom.Y, 0);

        Vector3 pP = Player.transform.position;
        meleeWeapon.transform.position = new Vector3(pP.x - 5, pP.y, pP.z);
        rangedWeapon.transform.position = new Vector3(pP.x + 5, pP.y, pP.z);
        potion.transform.position = new Vector3(pP.x, pP.y - 5, pP.z);
        
    }
    
    public void OnPlayerEnterRoom(Room room) {
        CameraController.Instance.CurrentRoom = room;
        currentRoom = room;
    }

    private void SetWallColliderTag(GameObject room) {
        Transform colliers = room.transform.GetChild(1);
        for (int child = 0; child < colliers.childCount; child++) {
            colliers.GetChild(child).tag = "Wall";
        }
    }

    public Dictionary<Room, GameObject> DungeonRoomsMap => dungeonRoomsMap;
}
