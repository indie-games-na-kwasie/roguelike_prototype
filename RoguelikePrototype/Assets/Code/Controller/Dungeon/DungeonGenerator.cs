﻿using System.Collections.Generic;
using UnityEngine;

public class DungeonGenerator : MonoBehaviour {
    
    [SerializeField] private DungeonGenerationData dungeonGenerationData = null;

    private List<Vector2Int> dungeonCrawlersMoves;

    private void Awake() {
        dungeonCrawlersMoves = DungeonCrawlerManager.GenerateDungeon(dungeonGenerationData);
        RoomManager.Instance.RoomsInDungeonCoordinates = new List<Vector2Int>(dungeonCrawlersMoves);
    }
    
}
