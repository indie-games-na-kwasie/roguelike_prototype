﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class RoomController : MonoBehaviour {
   
   [Header("Doors")]
   [SerializeField] private List<GameObject> doors = null;

   [Header("Sprites")]
   [SerializeField] private Sprite closedDoorSprite = null;
   [SerializeField] private Sprite openedDoorSprite = null;

  
   
   private List<GameObject> enemiesInRoom;
   private bool isAStartingRoom;
   private int enemyAmount;

   private void Awake() {
      enemyAmount = Random.Range(2, 5);
   }

   private void Start() {
      enemiesInRoom = new List<GameObject>();
      
      OpenRoom();
      
      if (!isAStartingRoom) {
         SpawnEnemies();
      }
   }
   
   public void OpenRoom() {
      foreach (var door in doors) {
         door.GetComponent<BoxCollider2D>().enabled = false;
         if (door.name.Equals("DOORS_N")) { door.GetComponent<SpriteRenderer>().sprite = openedDoorSprite; }
      }
   }

   public void CloseRoom() {
      if (enemyAmount != 0) {
         foreach (GameObject door in doors) {
            door.GetComponent<BoxCollider2D>().enabled = true;
            if (door.name.Equals("DOORS_N")) { door.GetComponent<SpriteRenderer>().sprite = closedDoorSprite; }
            Destroy(door.GetComponent<CapsuleCollider2D>());
         }
      }
   }

   public void UnlockRoom() {
      foreach (GameObject door in doors) { Destroy(door.GetComponent<CapsuleCollider2D>()); }
   }

   public void OnEnemyKilled() {
      if (--enemyAmount == 0) {
         OpenRoom();
      }
   }

   private void SpawnEnemies() {
      for (int i = 0; i < enemyAmount; i++) {
         float xPos = Random.Range(-11.5f, 11.5f);
         float yPos = Random.Range(-7.5f, 7.5f);
         Vector2 position = new Vector2(xPos, yPos);
         
         GameObject enemy = (GameObject) Instantiate(Resources.Load("Prefabs/Enemies/CULTIST"), transform.GetChild(2), true);
         enemy.transform.localPosition = position;
         enemiesInRoom.Add(enemy);
      }
   }

   private void OnTriggerEnter2D(Collider2D other) {
      if (!isAStartingRoom) {
         if (other.CompareTag("Player")) {
            isAStartingRoom = true;
         }
      }
   }

   public int EnemyAmount {
      get => enemyAmount;
      set => enemyAmount = value;
   }
}
