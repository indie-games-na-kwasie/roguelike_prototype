﻿using System;
using System.Collections;
using UnityEngine;

public enum EnemyState
{
    Stand,
    Follow,
    Die,
    Attack
}
public enum EnemyType
{
    Melee,
    Ranged
}

[Serializable]
public class EnemyController : MonoBehaviour
{
    private GameObject player;
    public EnemyState currState = EnemyState.Stand;
    public EnemyType enemyType;
    public float followRange;
    public float attackRange;
    public float cooldown;
    public float bulletSpeed;
    private bool coolDownAttack = false;
    public GameObject bulletPrefab;
    private int health = 50;

    private Vector2 movement = new Vector2(0,0);
    public Rigidbody2D enemyRigidbody;
    public float movementSpeed = 6f;
    public int Health { get => health; set => health = value; }

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        enemyRigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        switch (currState)
        {
            case (EnemyState.Stand):
                Stand();
                break;
            case (EnemyState.Follow):
                Follow();
                break;
            case (EnemyState.Die):
                break;
            case (EnemyState.Attack):
                Attack();
                break;

        }

        if(IsPlayerInRange(followRange) && currState != EnemyState.Die)
        {
            currState = EnemyState.Follow;
        }

        if(Vector3.Distance(transform.position, player.transform.position)<= attackRange)
        {
            currState = EnemyState.Attack;
        }
    }

    private void FixedUpdate()
    {
        enemyRigidbody.velocity = new Vector2(movement.x * movementSpeed, movement.y * movementSpeed);
    }

    private bool IsPlayerInRange(float followRange)
    {
        return Vector3.Distance(transform.position, player.transform.position) <= followRange;
    }

    void Stand() {
        if (IsPlayerInRange(followRange))
        {
            currState = EnemyState.Follow;
        }
    }

    void Follow()
    {
        transform.position = Vector2.MoveTowards(transform.position, player.transform.position, movementSpeed * Time.deltaTime);
        if (!IsPlayerInRange(followRange + 3))
        {
            currState = EnemyState.Stand;
        }
    }

    void Attack()
    {
        if (!coolDownAttack)
        {
            switch (enemyType)
            {
                case (EnemyType.Melee):
                    PlayerStatsController.DamagePlayer(10);
                    StartCoroutine(Cooldown());
                    break;
                case (EnemyType.Ranged):
                    GameObject bullet = Instantiate(bulletPrefab, transform.position, Quaternion.identity) as GameObject;
                    bullet.GetComponent<BulletController>().GetPlayer(player.transform);
                    bullet.AddComponent<Rigidbody2D>().gravityScale = 0;
                    bullet.GetComponent<BulletController>().isEnemyBullet = true;
                    StartCoroutine(Cooldown());
                    break;
            }
        }
    }

    private IEnumerator Cooldown()
    {
        coolDownAttack = true;
        yield return new WaitForSeconds(cooldown);
        coolDownAttack = false;
    }
   
    public void DamageEnemy(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Destroy(gameObject);
            transform.parent.transform.parent.GetComponent<RoomController>().OnEnemyKilled();
            EnemyManager.Instance.KillEnemy();
        }

    }
}
