﻿using System;
using UnityEngine;

public class EnemyManager : MonoBehaviour {
    private int leftEnemies = 0;

    public static EnemyManager Instance;
    
    private void Awake() {
        if (Instance == null) { Instance = this; }
    }

    public void GetEnemies() {
        foreach (GameObject room in RoomManager.Instance.DungeonRoomsMap.Values) {
            leftEnemies += room.GetComponent<RoomController>().EnemyAmount;
        }
        
        Debug.Log("All enemies: " + leftEnemies);
    }

    public void KillEnemy() {
        if (--leftEnemies == 0) {
            SceneRestart.Instance.WinGame();
        }
        
        Debug.Log("Left enemies: " + leftEnemies);
    }

}
