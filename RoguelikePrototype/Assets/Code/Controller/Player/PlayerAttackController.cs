﻿using UnityEngine;

public class PlayerAttackController : MonoBehaviour
{
    private float attackHorizontal;
    private float attackVertical;
    private Inventory inventory;
    public Transform attackPos;
    public float attackRange;
    public LayerMask whatIsEnemies;

    public Rigidbody2D playerRigidbody;
    public GameObject bulletPrefab;
    public float bulletSpeed = 5f;
    private float lastFire;
    public float fireDelay = 2f;

    private void Start()
    {
        playerRigidbody = GetComponent<Rigidbody2D>();
        inventory = GetComponent<Inventory>();
    }

    void Update()
    {
        float attackHor = Input.GetAxis("HorizontalAttack");
        float attackVer = Input.GetAxis("VerticalAttack");

        if ((attackHor != 0 || attackVer != 0) && Time.time > lastFire + fireDelay)
        {
            if(inventory.GetWeaponItem(PlayerStatsController.ChoosenSlot) != null && !inventory.GetWeaponItem(PlayerStatsController.ChoosenSlot).IsMeleWeapon)
            {
                PlayerStatsController.WeaponDamage = inventory.GetWeaponItem(PlayerStatsController.ChoosenSlot).WeaponDamage;
                rangedAttack(attackHor, attackVer);
                lastFire = Time.time;
            }
            else if (inventory.GetWeaponItem(PlayerStatsController.ChoosenSlot) != null && inventory.GetWeaponItem(PlayerStatsController.ChoosenSlot).IsMeleWeapon)
            {
                PlayerStatsController.WeaponDamage = inventory.GetWeaponItem(PlayerStatsController.ChoosenSlot).WeaponDamage;
                meleAttack(attackHor, attackVer);
                lastFire = Time.time;
            }
        }
        else if(attackHor == 0 && attackVer == 0)
        {
            attackPos.transform.position = playerRigidbody.position;
        }
    }

    private void meleAttack(float x, float y)
    {
        attackPos.transform.position = playerRigidbody.position + new Vector2(Mathf.Ceil(x), Mathf.Ceil(y));
        Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);
        for (int i = 0; i < enemiesToDamage.Length; i++)
        {
            enemiesToDamage[i].GetComponent<EnemyController>().DamageEnemy((int)PlayerStatsController.WeaponDamage);
        }
    }

    private void rangedAttack(float x, float y)
    {
        GameObject bullet = Instantiate(bulletPrefab, transform.position, transform.rotation) as GameObject;
        bullet.AddComponent<Rigidbody2D>().gravityScale = 0;
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector3(
            (x < 0) ? Mathf.Floor(x) * bulletSpeed : Mathf.Ceil(x) * bulletSpeed,
            (y < 0) ? Mathf.Floor(y) * bulletSpeed : Mathf.Ceil(y) * bulletSpeed,
            0
        );
    }
}
