﻿using UnityEngine;

public class PlayerStatsController : MonoBehaviour
{
    public static PlayerStatsController playerStatsController;
    private static UpdateHealth updateHealth = null;
    private static SceneRestart sceneRestart = null;
    private static int health = 50;
    private static int maxHealth = 100;
    private static int choosenSlot = 0;
    private static float moveSpeed = 7f;
    private static float attackRate = 0.5f;
    private static float bulletSize = 2f;
    private static float weaponDamage = 0;

    public static int Health { get => health; set => health = value; }
    public static int MaxHealth { get => maxHealth; set => maxHealth = value; }
    public static int ChoosenSlot { get => choosenSlot; set => choosenSlot = value; }
    public static float MoveSpeed { get => moveSpeed; set => moveSpeed = value; }
    public static float AttackRate { get => attackRate; set => attackRate = value; }
    public static float BulletSize { get => bulletSize; set => bulletSize = value; }
    public static float WeaponDamage { get => weaponDamage; set => weaponDamage = value; }

    private void Awake()
    {
        updateHealth = GameObject.FindGameObjectWithTag("HealthManager").GetComponent<UpdateHealth>();
        sceneRestart = GameObject.FindGameObjectWithTag("HealthManager").GetComponent<SceneRestart>();

        if (playerStatsController == null)
        {
            playerStatsController = this;
        }
    }

    public static void DamagePlayer(int damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Health = 0;
            KillPlayer();
        }
        updateHealth.updateHealthText();
    }

    public static void HealPlayer(int healAmount)
    {
        health = Mathf.Min(maxHealth, health + healAmount);
        updateHealth.updateHealthText();
    }

    private static void KillPlayer()
    {
        sceneRestart.ShowScreen();
        Time.timeScale = 0.0f;
    }

}
