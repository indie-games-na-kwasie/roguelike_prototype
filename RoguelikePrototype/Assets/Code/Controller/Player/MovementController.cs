﻿using UnityEngine;

public class MovementController : MonoBehaviour {
    
    [Header("References: ")]
    [SerializeField] private Rigidbody2D rb = null;
    [SerializeField] private Animator animator = null;
    
    [Header("Movement: ")]
    [SerializeField] private float baseMovementSpeed = 1.0f;
    
    private Vector2 movementDirection;
    private float movementSpeed;
    
    private void FixedUpdate() {
        ProcessInputs();
        Move();
        Animate();
    }

    private void ProcessInputs() {
        movementDirection = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
        movementSpeed = Mathf.Clamp(movementDirection.magnitude, 0.0f, 5f);
        movementDirection.Normalize();
    }

    private void Move() {
        rb.velocity = movementDirection * movementSpeed * baseMovementSpeed;
    }

    private void Animate() {
        if (movementDirection != Vector2.zero) {
            animator.SetFloat("Horizontal", movementDirection.x);
            animator.SetFloat("Vertical", movementDirection.y);
        }
        animator.SetFloat("Speed", movementSpeed);
    }
    
}
