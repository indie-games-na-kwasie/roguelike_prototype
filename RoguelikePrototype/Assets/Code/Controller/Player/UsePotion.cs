﻿using UnityEngine;

public class UsePotion : MonoBehaviour
{
    private Inventory inventory;
    void Start()
    {
        inventory = GetComponent<Inventory>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Q))
        {
            if (!inventory.HaveFreePotionSlot())
            {
                if(PlayerStatsController.Health < PlayerStatsController.MaxHealth)
                {
                    PlayerStatsController.HealPlayer(inventory.GetUsableItem(4).healthBack);
                    inventory.RemovePotion();
                    inventory.RefreshUI();
                }
            }
        }
    }
}
