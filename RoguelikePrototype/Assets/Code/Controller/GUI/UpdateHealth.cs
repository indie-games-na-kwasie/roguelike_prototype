﻿using UnityEngine;
using TMPro;

public class UpdateHealth : MonoBehaviour
{
    public TextMeshProUGUI healthText = null;

    public void updateHealthText()
    {
        healthText.text = "Health: " + PlayerStatsController.Health;
    }

    private void Awake()
    {
        healthText.text = "Health: " + PlayerStatsController.Health;
    }
}
