﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneRestart : MonoBehaviour {
    public static SceneRestart Instance;
    
    private Scene scene;
    
    [Header("Popups")]
    [SerializeField] private GameObject restartScreen = null;
    [SerializeField] private GameObject winScreen = null;

    private void Awake() {
        if (Instance == null) { Instance = this; }
    }

    private void Start()
    {
        scene = SceneManager.GetSceneByName("MainScene");
    }

    public void ReloadGame()
    {
        SceneManager.LoadScene(scene.name);
        ResetPlayerStats();
        HideScreen();
        CameraCollider.firstRoom = true;
        Time.timeScale = 1.0f;
    }

    public void StartNewGame() {
        SceneManager.LoadScene(scene.name);
        ResetPlayerStats();

        CameraCollider.firstRoom = true;
        Time.timeScale = 1.0f;
        winScreen.SetActive(false);
    }

    public void ResetPlayerStats()
    {
        PlayerStatsController.Health = 50;
        PlayerStatsController.ChoosenSlot = 0;
        PlayerStatsController.WeaponDamage = 0;
    }

    public void ShowScreen()
    {
        restartScreen.SetActive(true);
    }

    public void HideScreen()
    {
        restartScreen.SetActive(false);
    }

    public void WinGame() {
        winScreen.SetActive(true);
        Time.timeScale = 0;
    }
}
