﻿using UnityEngine;
using TMPro;

public class TextGUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI pickUpText = null;
    private Inventory inventory;

    private void Start()
    {
        pickUpText = GameObject.FindGameObjectWithTag("PickUpText").GetComponent<TextMeshProUGUI>();
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        pickUpText.gameObject.SetActive(false);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Weapon"))
        {
            if (!inventory.HaveFreeSlot())
            {
                pickUpText.text = "Press E to switch item with active slot";
            }
            else
            {
                pickUpText.text = "Press E to pick up item";
            }
            pickUpText.gameObject.SetActive(true);
        }

        if (collision.CompareTag("Potion"))
        {
            if (!inventory.HaveFreePotionSlot())
            {
                pickUpText.text = "Press E to switch potion";
            }
            else
            {
                pickUpText.text = "Press E to pick up potion";
            }
            pickUpText.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Weapon") || collision.CompareTag("Potion"))
        {
            pickUpText.gameObject.SetActive(false);
        }
    }
}
