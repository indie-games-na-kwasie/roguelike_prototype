﻿using UnityEngine;

public class ItemPickUp : MonoBehaviour
{
    [SerializeField] private Item item = null;
    [SerializeField] private Inventory inventory;

    private SpriteRenderer spriteRenderer;
    private bool invPickUpAllowed = false;
    private Item oldItem = null;

    private void Start()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<Inventory>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = item.ItemImage;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && invPickUpAllowed)
        {
            PickUpItem();
            invPickUpAllowed = false;
        }        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            invPickUpAllowed = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            invPickUpAllowed = true;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            invPickUpAllowed = false;
        }
    }

    private void RefreshItemSprite() {
        spriteRenderer.sprite = item.ItemImage;
    }

    public void PickUpItem()
    {
        if(item != null && inventory != null)
        {
            if(item.itemType == ItemTypeEnum.WEAPON)
            {
                if (inventory.HaveFreeSlot()) {
                    inventory.AddItem(item);
                    inventory.RefreshUI();
                    Destroy(gameObject);
                }
                else if (!inventory.HaveFreeSlot())
                {
                    oldItem = inventory.GetOldItemForSwitch();
                    inventory.SwitchItem(item);
                    item = oldItem;
                    inventory.RefreshUI();
                    RefreshItemSprite();
                    invPickUpAllowed = true;
                }
            }
            if(item.itemType == ItemTypeEnum.USABLE)
            {
                if (inventory.HaveFreePotionSlot())
                {
                    inventory.AddPotion(item);
                    inventory.RefreshUI();
                    Destroy(gameObject);
                }
                else if(!inventory.HaveFreePotionSlot())
                {
                    oldItem = inventory.GetOldPotionForSwitch();
                    inventory.SwitchPotion(item);
                    inventory.RefreshUI();
                    item = oldItem;
                    RefreshItemSprite();
                }
            }
        }
        invPickUpAllowed = false;
    }
}
