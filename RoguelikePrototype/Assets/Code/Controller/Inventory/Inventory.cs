﻿using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Item[] items;
    [SerializeField] private GameObject inventoryParent = null;
    [SerializeField] private ItemSlot[] itemSlots = null;
    [SerializeField] private GameObject[] inventorySlots = null;

    private ActiveSlot activeSlot;

    private int oldInventorySlotNumber = 255;
    private int switchSlot;
    private bool firstPickUp;

    private void OnValidate()
    {
        if (inventoryParent != null)
        {
            itemSlots = inventoryParent.GetComponentsInChildren<ItemSlot>();
            activeSlot = GetComponent<ActiveSlot>();
            items = new Item[itemSlots.Length];
        }
        inventorySlots = GameObject.FindGameObjectsWithTag("ItemGUISlot");
        firstPickUp = true;
    }

    public void RefreshUI()
    {
        int i = 0;
        for (; i < itemSlots.Length; i++)
        {
            itemSlots[i].Item = items[i];
        }

        for (; i < itemSlots.Length - 1; i++)
        {
            itemSlots[i].Item = null;
        }
    }

    public void ShowChoosenSlotOnGUI(int slotNumber)
    {
        switchSlot = slotNumber;
        if(items[slotNumber] != null)
        {
            if (oldInventorySlotNumber != slotNumber)
            {
                inventorySlots[slotNumber].transform.Translate(0, 15, 0);
            }
            if (oldInventorySlotNumber != slotNumber && oldInventorySlotNumber != 255)
            {
                inventorySlots[oldInventorySlotNumber].transform.Translate(0, -15, 0);
            }
            oldInventorySlotNumber = slotNumber;
        }
    }

    public bool haveWeaponInSlot(int slotNumber)
    {
        if (items[slotNumber] != null)
        {
            return true;
        }
        else { 
            return false; 
        }
    }

    public bool HaveFreeSlot()
    {
        int freeSlots = 0;
        for (int i = 0; i < items.Length - 1; i++)
        {
            if(items[i] == null)
            {
                freeSlots++;
            }
        }

        if(freeSlots > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool HaveFreePotionSlot()
    {
        if (items[4] != null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void AddItem(Item item)
    {
        for (int i = 0; i < items.Length - 1; i++)
        {
            if(items[i] == null)
            {
                items[i] = item;
                if (firstPickUp)
                {
                    activeSlot.FirstPickUp();
                    firstPickUp = false;
                }
                break;
            }   
        }
    }

    public void SwitchItem(Item item)
    {
        items[switchSlot] = item;
    }

    public void AddPotion(Item item)
    {
        if(items[4] == null)
        {
            items[4] = item;
        }
    }

    public void RemovePotion()
    {
        items[4] = null;
    }

    public void SwitchPotion(Item item)
    {
        items[4] = item;
    }

    public Item GetOldItemForSwitch()
    {
        return items[switchSlot];
    }

    public Item GetOldPotionForSwitch()
    {
        return items[4];
    }

    public Weapon GetWeaponItem(int activeSlot)
    {
        return (Weapon)items[activeSlot];
    }

    public UsableItem GetUsableItem(int activeSlot)
    {
        return (UsableItem)items[4];
    }
}
