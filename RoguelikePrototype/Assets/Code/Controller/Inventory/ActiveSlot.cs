﻿using UnityEngine;

public class ActiveSlot : MonoBehaviour
{
    private Inventory inventory;

    private void Start()
    {
        inventory = GetComponent<Inventory>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (inventory.haveWeaponInSlot(0))
            {
                ChooseSlot(1);
                PlayerStatsController.ChoosenSlot = 0;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            if (inventory.haveWeaponInSlot(1))
            {
                ChooseSlot(2);
                PlayerStatsController.ChoosenSlot = 1;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            if (inventory.haveWeaponInSlot(2))
            {
                ChooseSlot(3);
                PlayerStatsController.ChoosenSlot = 2;
            }
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            if (inventory.haveWeaponInSlot(3))
            {
                ChooseSlot(4);
                PlayerStatsController.ChoosenSlot = 3;
            }
        }
    }

    private void ChooseSlot(int playerInput)
    {
        switch (playerInput)
        {
            case 1:
                inventory.ShowChoosenSlotOnGUI(0);
                break;
            case 2:
                inventory.ShowChoosenSlotOnGUI(1);
                break;
            case 3:
                inventory.ShowChoosenSlotOnGUI(2);
                break;
            case 4:
                inventory.ShowChoosenSlotOnGUI(3);
                break;
            default:
                inventory.ShowChoosenSlotOnGUI(0);
                break;
        }
    }

    public void FirstPickUp()
    {
        ChooseSlot(1);
    }
}
